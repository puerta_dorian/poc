import {Component, Inject, OnInit} from '@angular/core';
import {DisplayFileContentService} from "../services/display-file-content.service";
import {DisplayFile} from "../interface/display-file";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-file-content',
  templateUrl: './file-content.component.html',
  styleUrls: ['./file-content.component.scss']
})
export class FileContentComponent implements OnInit {
  fileContent = "";

  constructor(private displayFileContentService: DisplayFileContentService,
              public dialogRef: MatDialogRef<FileContentComponent>, @Inject(MAT_DIALOG_DATA) public data: DisplayFile) {
  }

  ngOnInit(): void {
    console.log('data', this.data);
    this.displayFileContentService.getFileContent(this.data.filename).subscribe(result => {
      this.fileContent = result;
    });
  }

  close() {
    this.dialogRef.close()
  }


}
