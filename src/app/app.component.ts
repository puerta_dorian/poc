import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poc';
  step: number | null = null;
  cardTitle = [
    'Select copybook',
    'Select associated flat files',
    'Generate JSON File',
    'Done'
  ];
  fileCopyInfo: any;
  fileFlatFileInfo: any;
  toggleSearch = false;

  stepIncrement() {
    if (!this.step) {
      this.step = 0;
    }
    this.step += 1;
    console.group('Form View-Model')
    console.log('Input File Copybook', this.fileCopyInfo);
    console.log('Input file flatfile', this.fileFlatFileInfo);
    console.groupEnd();
  }

  removeItem(item: any) {

  }

  toggleFileSearch(): void {
    this.toggleSearch = !this.toggleSearch;
  }
}
