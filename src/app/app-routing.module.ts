import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CopybookConversionComponent} from "./copybook-conversion/copybook-conversion.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [{path: '', component: HomeComponent}, {
  path: 'copybookConversion',
  component: CopybookConversionComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
