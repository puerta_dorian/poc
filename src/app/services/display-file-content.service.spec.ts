import { TestBed } from '@angular/core/testing';

import { DisplayFileContentService } from './display-file-content.service';

describe('DisplayFileContentService', () => {
  let service: DisplayFileContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DisplayFileContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
