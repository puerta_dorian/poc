import { TestBed } from '@angular/core/testing';

import { CopybookConversionService } from './copybook-conversion.service';

describe('CopybookConversionService', () => {
  let service: CopybookConversionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CopybookConversionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
