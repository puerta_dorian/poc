import {Injectable} from '@angular/core';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import {Observable} from "rxjs";
import {FileItem} from "../models/file-item";

@Injectable({
  providedIn: 'root'
})
export class FileIoService {

  constructor() {
  }

  fileZipAttachment(files: Array<FileItem>,zipFileName:string): void {
    var zip = new JSZip();
    var count = 0;

    files.forEach(function (item) {
      // loading a file and add it in a zip file
        zip.file(item.filename, item.content, {binary:true});
        count++;
        if (count == files.length) {
          zip.generateAsync({type:'blob'}).then(function(content) {
            // call download prompt
            saveAs(content, zipFileName);
          });
        }
    });
  }


  readFileContent(file: any): Observable<any> {
    return new Observable(subscriber => {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        subscriber.next(fileReader.result);
        subscriber.complete();
      }
      fileReader.readAsText(file);
    });
  }

}
