import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DisplayFile} from "../interface/display-file";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DisplayFileContentService {

  constructor(private http: HttpClient) {
  }

  getFileContent(fileName: string) {
    return this.http.get(`assets/${fileName}`, {responseType: 'text'});
  }

  getFiles(): Observable<Array<DisplayFile>> {
    return this.http.get<any>(`assets/data.json`).pipe(map(i => i.data));
  }
}
