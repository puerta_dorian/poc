import { Injectable } from '@angular/core';
import * as txml from 'txml';

@Injectable({
  providedIn: 'root'
})
export class CopybookConversionService {

  constructor() { }

  convertToJson(content:string) {
    return txml.parse(content);
  }
}
