import { TestBed } from '@angular/core/testing';

import { FileIoService } from './file-io.service';

describe('FileIoService', () => {
  let service: FileIoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileIoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
