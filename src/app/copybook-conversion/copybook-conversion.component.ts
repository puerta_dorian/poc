import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {FileIoService} from "../services/file-io.service";

@Component({
  selector: 'app-copybook-conversion',
  templateUrl: './copybook-conversion.component.html',
  styleUrls: ['./copybook-conversion.component.scss']
})
export class CopybookConversionComponent implements OnInit {
  step: number | null = null;
  fileCopyInfo: any;
  fileFlatFileInfo: any;
  toggleSearch = false;
  constructor(private fileIoService:FileIoService) {
  }

  ngOnInit() {
  }

  stepIncrement() {
    if (!this.step) {
      this.step = 0;
    }
    this.step += 1;
    console.group('Form View-Model')
    console.log('Input File Copybook', this.fileCopyInfo);
    console.log('Input file flatfile', this.fileFlatFileInfo);
    console.groupEnd();
  }

  removeItem(item: any) {

  }

  toggleFileSearch(): void {
    this.toggleSearch = !this.toggleSearch;
  }

  convert(): void {
    this.fileIoService.readFileContent(this.fileCopyInfo).subscribe(result => {
      console.log('read copybook file', result)
      this.step = 3;
    });
  }

}
