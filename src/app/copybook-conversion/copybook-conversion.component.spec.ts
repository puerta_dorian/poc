import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CopybookConversionComponent } from './copybook-conversion.component';

describe('CopybookConversionComponent', () => {
  let component: CopybookConversionComponent;
  let fixture: ComponentFixture<CopybookConversionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CopybookConversionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopybookConversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
