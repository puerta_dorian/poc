import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonSearchComponent } from './json-search.component';

describe('JsonSearchComponent', () => {
  let component: JsonSearchComponent;
  let fixture: ComponentFixture<JsonSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JsonSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
