import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FileContentComponent} from "../file-content/file-content.component";
import {MatDialog} from "@angular/material/dialog";
import {DisplayFile} from "../interface/display-file";
import {DisplayFileContentService} from "../services/display-file-content.service";
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";

@Component({
  selector: 'app-json-search',
  templateUrl: './json-search.component.html',
  styleUrls: ['./json-search.component.scss']

})
export class JsonSearchComponent implements OnInit {
  dataRows!: Array<DisplayFile>;
  columns :Array<any>= [];
  ColumnMode = ColumnMode;
  temp: Array<DisplayFile> = [];
  @Input('rows') rows = true;
  @ViewChild(DatatableComponent) table!: DatatableComponent;
  @ViewChild('viewTemplate', {static: true}) viewTemplate!: TemplateRef<any>;

  constructor(public dialog: MatDialog, private displayFileContentService: DisplayFileContentService) {

  }

  ngOnInit(): void {
    this.displayFileContentService.getFiles().subscribe((results: Array<DisplayFile>) => {
      console.log('results', results)
      this.temp = [...results];
      this.dataRows = results;
    });
    this.columns = [{name: 'Previous File Conversions', prop: 'filename', cellTemplate: this.viewTemplate}]
  }


  updateFilter(event: any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.dataRows = this.temp.filter(d => {
      return d.filename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  openDialog(data: any): void {
    console.log('data',data)
    const dialogRef = this.dialog.open(FileContentComponent, {
      width: '250px',
      data: {filename:data}
    });
  }
}
