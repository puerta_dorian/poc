export class FileItem {
  filename: string = "";
  content: string = "";

  public constructor(init?: Partial<FileItem>) {
    Object.assign(this, init);
  }
}
